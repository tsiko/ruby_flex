/* Onoma arxeiou:	simple-parser.y
   Perigrafh:		Aplos syntaktikos analyths (kwdikas gia to ergaleio Bison)
   Syggrafeas:		Ergasthrio Metaglwttistwn, Tmhma Mhx.Plhroforikhs TE, TEI Athhnas
   Sxolia:		O syntaktikos analyths anagnwrizei monaxa:
				1) Dhlwsh metablhths typou integer =========> int a;
				2) Anathesh timhs akeraiou se metablhth ====> a = 5;
				3) Anathesh timhs metablthhs se metablhth ==> a = b;
			Katanoei kai xrhsimopoiei stous grammatikous kanones tis akolouthes
			lektikes monades pou parexontai/anagnwrizontai apo to ergaleio Flex:
				1) SINT		H leksh "int" gia orismo metablhths integer
				2) SEMI		O xarakthras ';' ws termatikos entolhs
				3) ASSIGNOP	O xarakthras '=' gia tis anatheseis timwn
				4) IDENTIFIER	Anagnwristiko / onoma metablhths
				5) INTCONST	Akeraios arithmos
*/

%{

/* --------------------------------------------------------------------------------
   Orismoi kai dhlwseis glwssas C. Otidhpote exei na kanei me orismo h arxikopoihsh
   metablhtwn, arxeia header kai dhlwseis #define mpainei se auto to shmeio */

#include <stdio.h>
#include <string.h>
int line=0;
int errflag=0;
char lineText[50];
extern char *yytext;
#define YYSTYPE char *

%}

/* -----------------------------
   Dhlwseis kai orismoi Bison */

/* Orismos twn anagnwrisimwn lektikwn monadwn. */
//SUMVOLA KAI TELESTES
%token PLUS MINUS MUL DIV MOD ASSIGN POWER LOR LAND EQUAL DOUBLE_DOTS LSR LSL LPARENTHESIS RPARENTHESIS BACKSLASH QUOTE DOUBLEQUOTE GRATERTHAN LOWERTHAN DOT COMMA NEWLINE
// METAVLHTES ARI8MOI METHODOI
%token INTCONST FLOAT METHOD LOCAL_V CONST_V GLOBAL_V CLASS_V INST_V STRING CHAR CLASS_NAMEorV METHODorV
//KRATIMENES LEKSEIS
%token CLASS IF ELSIF ELSE UNLESS UNTIL RETURN NEXT BEGIN2 END BREAK DEF DO
//PRWTARXIKES SUNARTISEIS
%token PRINT PUTS GETS ABORT CHOMP OPEN LOOP
//ERRORS
%token TOKEN_ERROR INT_ERROR INT_ERROR2 INT_ERROR3 FLOAT_ERROR FLOAT_ERROR2 FLOAT_ERROR3 METHOD_ERROR METHOD_ERROR2 METHOD_ERROR3 METHOD_ERROR4 LOCAL_V_ERROR LOCAL_V_ERROR2 LOCAL_V_ERROR3 CONST_V_ERROR CONST_V_ERROR2 CONST_V_ERROR3 GLOBAL_V_ERROR GLOBAL_V_ERROR2 GLOBAL_V_ERROR3 INST_V_ERROR INST_V_ERROR2 INST_V_ERROR3 CLASS_V_ERROR CLASS_V_ERROR2 CLASS_V_ERROR3 STRING_ERROR CHAR_ERROR CHAR_ERROR2

%left NEWLINE
%left COMMA
%right ASSIGN
%left LOR
%left LAND
%left EQUAL
%left GRATERTHAN LOWERTHAN
%left LSR LSL
%left PLUS MINUS
%left MUL DIV MOD
%right POWER
%left LPARENTHESIS RPARENTHESIS

/* Orismos tou symbolou enarkshs ths grammatikhs */
%start program

%%

/* --------------------------------------------------------------------------------
   Orismos twn grammatikwn kanonwn. Kathe fora pou antistoixizetai enas grammatikos
   kanonas me ta dedomena eisodou, ekteleitai o kwdikas C pou brisketai anamesa sta
   agkistra. H anamenomenh syntaksh einai:
				onoma : kanonas { kwdikas C } */
					    
program	: program assign 
	| program method_created 
	| program method 
	| program class2 
	| program condition 
	| program until2 
	| program if_end 
	| program begin2
	| program ABORT 
	| program gets 
	| program print 
	| program puts 
	| program open
	| program loop2 
	| program return 
	| program NEWLINE
	|
;

operator: PLUS 
	| MINUS
	| MUL 
	| DIV 
	| MOD 
	| POWER 
	| LOR 
	| LAND 
	| LSR 
	| LSL 
;

relational_op : EQUAL 
	| GRATERTHAN 
	| LOWERTHAN 
;

return: RETURN value NEWLINE 
	| RETURN variable NEWLINE 
	| RETURN expresion NEWLINE 
;



value : INTCONST 
	| FLOAT 
	| CHAR 
	| STRING 
	| INT_ERROR {printf("\n\t### Line:%d FAIL : %d \n", line, INT_ERROR);strcpy(lineText,"");}
	| INT_ERROR2 {printf("\n\t### Line:%d FAIL : %d \n", line, INT_ERROR2);strcpy(lineText,"");}
	| INT_ERROR3 {printf("\n\t### Line:%d FAIL : %d \n", line, INT_ERROR3);strcpy(lineText,"");}
	| FLOAT_ERROR   {printf("\n\t### Line:%d FAIL : %d \n", line, FLOAT_ERROR);strcpy(lineText,"");}
	| FLOAT_ERROR2   {printf("\n\t### Line:%d FAIL : %d \n", line, FLOAT_ERROR2);strcpy(lineText,"");}
	| FLOAT_ERROR3   {printf("\n\t### Line:%d FAIL : %d \n", line, FLOAT_ERROR2);strcpy(lineText,"");}
;

variable : LOCAL_V 
	| CONST_V 
	| GLOBAL_V 
	| CLASS_V 
	| INST_V 
	| METHODorV 
	| CLASS_NAMEorV 
	| METHOD_ERROR2   {printf("\n\t### Line:%d FAIL : %d \n", line, METHOD_ERROR2);strcpy(lineText,"");}
 	| METHOD_ERROR3   {printf("\n\t### Line:%d FAIL : %d \n", line, METHOD_ERROR3);strcpy(lineText,"");}
	| METHOD_ERROR4   {printf("\n\t### Line:%d FAIL : %d \n", line, METHOD_ERROR4);strcpy(lineText,"");}

	| LOCAL_V_ERROR2   {printf("\n\t### Line:%d FAIL : %d \n", line, LOCAL_V_ERROR2);strcpy(lineText,"");}

	| CONST_V_ERROR3   {printf("\n\t### Line:%d FAIL : %d \n", line, LOCAL_V_ERROR3);strcpy(lineText,"");}

	| GLOBAL_V_ERROR3   {printf("\n\t### Line:%d FAIL : %d \n", line, GLOBAL_V_ERROR3);strcpy(lineText,"");}
	| INST_V_ERROR3   {printf("\n\t### Line:%d FAIL : %d \n", line, INST_V_ERROR3);strcpy(lineText,"");}
;

// parametroi 	
param : LPARENTHESIS variable 
	|LPARENTHESIS assign 
	|param COMMA assign 
	|param COMMA variable 
;
	
param_complate : param RPARENTHESIS 
;

arguments : LPARENTHESIS variable 
	|LPARENTHESIS value 
	|arguments COMMA value 
	|arguments COMMA variable 
;
	
arguments2 : arguments RPARENTHESIS 
;


stmt: LPARENTHESIS condition RPARENTHESIS 
      | LPARENTHESIS stmt LAND stmt RPARENTHESIS
      | LPARENTHESIS stmt LOR stmt RPARENTHESIS
      | LPARENTHESIS variable relational_op value RPARENTHESIS
      | LPARENTHESIS "true" RPARENTHESIS
;
 
if_statement: IF stmt NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}    
	|if_statement ELSIF stmt NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	| if_statement assign 
	| if_statement until2 
	| if_statement begin2 
	| if_statement NEXT 
	| if_statement BREAK 
	| if_statement gets 
	| if_statement ABORT 
	| if_statement print 
	| if_statement puts 
	| if_statement open 
	| if_statement loop2 
	| if_statement if_end 
	| if_statement return 
	| if_statement NEWLINE 	      
	    ;

if2: if_statement ELSE NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	|if2 ELSIF stmt NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");} 
	| if2 assign 
	| if2 until2 
	| if2 begin2 
	| if2 NEXT
	| if2 BREAK 
	| if2 gets 
	| if2 ABORT 
	| if2 print 
	| if2 puts 
	| if2 open
	| if2 loop2 
	| if2 if_end 	
	| if2 return 	
	| if2 NEWLINE       
	    ;


if_end: if_statement END NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	| if2 END NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;

	


// dhlosh methododou
// arxh methodou
method_begin: DEF METHODorV NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	| DEF LOCAL_V param_complate NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;


//pros8hkh kodika sthn methodo
method_code: | method_begin 
	| method_code assign 
	| method_code method 
	| method_code if_end 
	| method_code gets 
	| method_code ABORT 
	| method_code print 
	| method_code puts 
	| method_code open
	| method_code loop2
	| method_code return 
	| method_code NEWLINE 
	
;

//telos methodou	
method_created : method_code END NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;

//klhsh methodou
method: METHODorV NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	|method COMMA value NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	|method COMMA variable NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	|method NEWLINE NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;

class: CLASS CLASS_NAMEorV NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	|class assign 
	| class method_created 
	| class method 
	| class class 
	| class if_end 
	| class condition 
	| class until2 
	| class begin2 
	| class ABORT 
	| class gets 
	| class print 
	| class puts 
	| class open 
	| class loop2 
	| class return 
	| class NEWLINE
;

class2: class END NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;

// sun8hkes
condition : variable relational_op value 
	|variable relational_op variable 
	| value relational_op value
	| value relational_op variable
;

//praxeis	
expresion: value operator value
	| value operator variable 
	| variable operator value 
	| variable operator variable 
	| expresion operator expresion
	| expresion operator value 
	| expresion operator variable 
	| LPARENTHESIS expresion RPARENTHESIS 
;

//ekxwrish se metavlhth
assign	: variable ASSIGN value NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");strcpy(lineText,"");}
	| variable ASSIGN variable NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");strcpy(lineText,"");}
	| variable ASSIGN expresion NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");strcpy(lineText,"");}
;

until: UNTIL condition DO NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	| until assign
	| until until2
	| until begin2
	| until method
	| until class
	| until if_end 
	| until NEXT
	| until BREAK
	| until gets
	| until ABORT
	| until print
	| until puts
	| until open 
	| until loop2 
	| until return
	| until NEWLINE 
;

until2: until END NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;

begin: BEGIN2 NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	| begin assign 
	| begin until2 
	| begin begin2 
	| begin method 
	| begin class 
	| begin if_end 
	| begin NEXT 
	| begin BREAK 
	| begin gets 
	| begin ABORT 
	| begin print 
	| begin puts 
	| begin open 
	| begin loop2 
	| begin return 
	| begin NEWLINE 
;
begin2: begin END UNTIL condition NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;

print: PRINT arguments2 NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
; 
puts: PUTS LPARENTHESIS STRING RPARENTHESIS NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	| PUTS LPARENTHESIS variable RPARENTHESIS NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;
gets: GETS NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);}
	| GETS DOT CHOMP NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);}
;
open: OPEN LPARENTHESIS variable COMMA STRING RPARENTHESIS NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;
loop: LOOP DO NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
	| loop assign 
	| loop until2 
	| loop begin2 
	| loop method 
	| loop class 
	| loop if_end 
	| loop NEXT
	| loop BREAK 
	| loop gets 
	| loop ABORT 
	| loop puts 
	| loop print 
	| loop open 
	| loop loop2 
	| loop return 
	| loop NEWLINE 
;
loop2: loop END NEWLINE {printf("\n\t### Line:%d SUCCESS : %s \n", line, lineText);strcpy(lineText,"");}
;

		
%%

/* --------------------------------------------------------------------------------
   Epiprosthetos kwdikas-xrhsth se glwssa C. Sto shmeio auto mporoun na prostethoun
   synarthseis C pou tha symperilhfthoun ston kwdika tou syntaktikoy analyth */


/* H synarthsh yyerror xrhsimopoieitai gia thn anafora sfalmatwn. Sygkekrimena kaleitai
   apo thn yyparse otan yparksei kapoio syntaktiko lathos. Sthn parakatw periptwsh h
   synarthsh epi ths ousias den xrhsimopoieitai kai aplws epistrefei amesws. */

int yyerror(void)
{}


/* O deikths yyin einai autos pou "deixnei" sto arxeio eisodou. Ean den ginei xrhsh
   tou yyin, tote h eisodos ginetai apokleistika apo to standard input (plhktrologio) */

FILE *yyin;


/* H synarthsh main pou apotelei kai to shmeio ekkinhshs tou programmatos.
   Ginetai elegxos twn orismatwn ths grammhs entolwn kai klhsh ths yyparse
   pou pragmatopoiei thn syntaktikh analysh. Sto telos ginetai elegxos gia
   thn epityxh h mh ekbash ths analyshs. */

int main(int argc,char **argv)
{
	int i;
	if(argc == 2)
		yyin=fopen(argv[1],"r");
	else
		yyin=stdin;

	strcpy(lineText," ");

	int parse = yyparse();

	if (errflag==0 && parse==0)
		printf("\nINPUT FILE: PARSING SUCCEEDED. :%d\n", parse);
	else
		printf("\nINPUT FILE: PARSING FAILED. :%d\n", parse);

	return 0;
}
