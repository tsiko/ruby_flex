/* Onoma arxeiou:	scanner.l
   Perigrafh:		Aplos lektikos analyths me xrhsh tou ergaleiou Flex:
			Paradeigma gia anagnwrish lathous se akeraious
   Syggrafeas:		Ergasthrio Metaglwttistwn, Tmhma Mhx.Plhroforikhs TE, TEI Athhnas
   Sxolia:		To paron programma ylopoiei (me th xrhsh Flex) enan aploiko lektiko
			analyth pou anagnwrizei: to '+', delimiters (space kai tab), newline,
			akeraious sthn aplh morfh [0-9]+ kai lanthasmenous akeraious pou
			teleiwnoun me kapoio gramma.
   Odhgies ekteleshs:	flex scanner.l
			gcc lex.yy.c -lfl -o scanner
			./scanner
*/

%option noyywrap
%x error

/* Kwdikas C gia orismo twn apaitoumenwn header files kai twn metablhtwn.
   Otidhpote anamesa sta %{ kai %} metaferetai autousio sto arxeio C pou
   tha dhmiourghsei to Flex. */

%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Header file pou periexei lista me ola ta tokens */
#include "token.h"

/* Orismos metrhth trexousas grammhs */
int line=1;

void ERROR(const char *msg);
void prn(char *s);
void found(int token);

%}

/* Onomata kai antistoixoi orismoi (ypo morfh kanonikhs ekfrashs).
   Meta apo auto, mporei na ginei xrhsh twn onomatwn (aristera) anti twn,
   synhthws idiaiterws makrsokelwn kai dysnohtwn, kanonikwn ekfrasewn */

DELIMITERS	[ \t]+
FLOAT           [+-]?([1-9][0-9]*|0)[,][0-9]*
INTEGER 	[+-]?[1-9][0-9]*|0
METHOD          [a-z][a-zA-Z0-9]*
LOCAL_V         [a-z][a-zA-Z0-9_]*[a-zA-Z0-9]
CONST_V         [A-Z][a-zA-Z0-9_]*[a-zA-Z0-9]
GLOBAL_V        [$][a-zA-Z0-9_]*[a-zA-Z0-9]
CLASS_V         [@][a-zA-Z0-9_]*[a-zA-Z0-9]
INST_V          [@@][a-zA-Z0-9_]*[a-zA-Z0-9]
COMMENT         (=begin(.|[\n])*=end)|(#.*)
STRING 		["][a-z][a-zA-Z0-9_]*[a-zA-Z0-9]["]
INTEGER_ERROR	{INTEGER}[A-Za-z]+
FLOAT_ERROR     {FLOAT}[A-Za-z]+


/* Gia kathe pattern (aristera) pou tairiazei ekteleitai o antistoixos
   kwdikas mesa sta agkistra. */


%%
"+"		{prn("PLUS"); return PLUS;}
{DELIMITERS}	{}

{LOCAL_V}	{prn("Local Variable"); return LOCAL_V;}
{CONST_V}	{prn("Constant Variable"); return CONST_V;}
{GLOBAL_V}	{prn("Global Variable"); return CONST_V;}
{STRING}	{prn("string"); return STRING;}
{COMMENT}       {prn("comment");}
\n 		{line++;}
{INTEGER}	{prn("INTCONST"); return INTCONST;}
{FLOAT}		{prn("Float"); return FLOAT;}
{INTEGER_ERROR}	{ERROR("Integer ends with letter!"); BEGIN(error); return TOKEN_ERROR;}
{FLOAT_ERROR}	{ERROR("Float ends with letter!"); BEGIN(error); return TOKEN_ERROR;}
.		{ERROR("Unrecognized token error!"); BEGIN(error); return TOKEN_ERROR;}
<error>[ \t\n]	{BEGIN(0);}
<error>.	{}
<<EOF>>		{ printf("#End of file#\n"); exit(0); }
%%
int main(int argc, char **argv){
	int token;

        /* Ginetai o elegxos twn orismatwn ths grammhs entolwn. Ean ta
           orismata einai 3, to programma diabazei apo to arxeio tou 2ou
           orismatos kai grafei sto arxeio tou 3ou. Ean ta orismata einai
           2 diabazei apo to arxeio tou 2ou kai grafei sthn othonh.
           Ypenthymizetai oti to 1o orisma (argv[0]) sth C einai to onoma
           tou idiou tou ektelesimou arxeiou. */

	if(argc == 3){
		if(!(yyin = fopen(argv[1], "r"))) {
			fprintf(stderr, "Cannot read file: %s\n", argv[1]);
			return 1;
		}
		if(!(yyout = fopen(argv[2], "w"))) {
			fprintf(stderr, "Cannot create file: %s\n", argv[2]);
			return 1;
		}
	}
	else if(argc == 2){
		if(!(yyin = fopen(argv[1], "r"))) {
			fprintf(stderr, "Cannot read file: %s\n", argv[1]);
			return 1;
		}
	}

	/* H synarthsh yylex diabazei xarakthres apo thn eisodo kai prospathei
           na angnwrisei tokens. Ta tokens pou anagnwrizei einai auta pou exoun
           oristei sto paron arxeio, anamesa sta %% kai %%. An o kwdikas pou
           antistoixei se kapoio pattern periexei thn entolh 'return TIMH', h
	   yylex() epistrefei thn timh auth h opoia kai apothhkeyetai sth
	   metablhth token. */

	while( (token=yylex()) >= 0){
        	/* Gia kathe token pou anagnwristhke, ektypwnetai h grammh pou auto
		   brethhke kathws kai to onoma tou mazi me thn timh tou. */
		fprintf(yyout, "\tLine=%d,  value=\"%s\"\n", line, yytext);
	}
	return 0;
}
/* H synarthsh typwnei sthn eksodo ena mhnyma lathous sthn periptwsh pou anagnwristei
   lanthasmeno token opws px. akeraios pou teleiwnei me gramma. */
void ERROR(const char *msg)
{
        fprintf(yyout, "\tFlex -> ERROR, line %d at lexeme \'%s\' : Token Error: %s\n",line, yytext, msg);
}

/* H synarthsh typwnei sthn othonh to keimeno tou anagnwristikou pou tairiakse me kapoion
   kanona. Xrhsimopoieitai kathara gia logous katanohshs kai debugging. */
void prn(char *s)
{

	printf("\tFlex -> Matched token: %s is %s at line %d\n", yytext,s,line);
}






