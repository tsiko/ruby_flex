/* Onoma arxeiou:   scanner.l
   Perigrafh:       Aplos lektikos analyths me xrhsh tou ergaleiou Flex:
            Paradeigma gia anagnwrish lathous se akeraious
   Syggrafeas:      Ergasthrio Metaglwttistwn, Tmhma Mhx.Plhroforikhs TE, TEI Athhnas
   Sxolia:      To paron programma ylopoiei (me th xrhsh Flex) enan aploiko lektiko
            analyth pou anagnwrizei: to '+', delimiters (space kai tab), newline,
            akeraious sthn aplh morfh [0-9]+ kai lanthasmenous akeraious pou
            teleiwnoun me kapoio gramma.
   Odhgies ekteleshs:   flex scanner.l
            gcc lex.yy.c -lfl -o scanner
            ./scanner
*/

%option noyywrap
%x error
%x comment
%x string
%x character

/* Kwdikas C gia orismo twn apaitoumenwn header files kai twn metablhtwn.
   Otidhpote anamesa sta %{ kai %} metaferetai autousio sto arxeio C pou
   tha dhmiourghsei to Flex. */

%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Header file pou periexei lista me ola ta tokens */
#include "token.h"

/* Orismos metrhth trexousas grammhs */
int line=1;

void ERROR(const char *msg,int t);
void prn(char *s,int t);
void found(int token);

%}

/* Onomata kai antistoixoi orismoi (ypo morfh kanonikhs ekfrashs).
   Meta apo auto, mporei na ginei xrhsh twn onomatwn (aristera) anti twn,
   synhthws idiaiterws makrsokelwn kai dysnohtwn, kanonikwn ekfrasewn */

DELIMITERS  [ \t]+
FLOAT           [+-]?([1-9][0-9]*|0)[.][0-9]*
INTEGER     [+-]?[1-9][0-9]*|0
METHOD          [a-z][a-zA-Z0-9]*
LOCAL_V         [a-z][a-zA-Z0-9_]*[a-zA-Z0-9]|[a-z]
CONST_V         [A-Z][a-zA-Z0-9_]*[a-zA-Z0-9]|[A-Z]
GLOBAL_V        $[a-zA-Z][a-zA-Z0-9_]*[a-zA-Z0-9]|$[a-zA-Z]
INST_V          @@[a-zA-Z][a-zA-Z0-9_]*[a-zA-Z0-9]|@@[a-zA-Z]
CLASS_V         @[a-zA-Z][a-zA-Z0-9_]*[a-zA-Z0-9]|@[a-zA-Z]
LINE_COMMENT    #.*

INTEGER_ERROR   {INTEGER}[A-Za-z]+
INT_ERROR2	[+-]{INTEGER}
INT_ERROR3	0{INTEGER}

FLOAT_ERROR     {FLOAT}[A-Za-z]+
FLOAT_ERROR2    [+-]{FLOAT}
FLOAT_ERROR3    0{FLOAT}

METHOD_ERROR    [A-Z][a-zA-Z0-9]*
METHOD_ERROR2    [0-9][a-zA-Z0-9]*
METHOD_ERROR3   [a-z_][a-zA-Z0-9_]*
METHOD_ERROR4	[.^a-z][a-zA-Z0-9_]*

LOCAL_V_ERROR    [A-Z][a-zA-Z0-9_]*[a-zA-Z0-9]|[A-Z]
LOCAL_V_ERROR2    [0-9][a-zA-Z0-9_]*[a-zA-Z0-9]|[0-9]
LOCAL_V_ERROR3   [.^a-z][a-zA-Z0-9_]*

CONST_V_ERROR    [a-z][a-zA-Z0-9_]*[a-zA-Z0-9]|[a-z]
CONST_V_ERROR2   [0-9][a-zA-Z0-9_]*[a-zA-Z0-9]|[0-9]
CONST_V_ERROR3   [.^A-Z][a-zA-Z0-9_]*

GLOBAL_V_ERROR    [A-Za-z][a-zA-Z0-9_]*[a-zA-Z0-9]|[A-Za-z]
GLOBAL_V_ERROR2   [0-9][a-zA-Z0-9_]*[a-zA-Z0-9]|[0-9]
GLOBAL_V_ERROR3   [.^$][a-zA-Z0-9_]*

INST_V_ERROR    [A-Za-z][a-zA-Z0-9_]*[a-zA-Z0-9]|[A-Za-z]
INST_V_ERROR2   [0-9][a-zA-Z0-9_]*[a-zA-Z0-9]|[0-9]
INST_V_ERROR3   [.^@@][a-zA-Z0-9_]*

CLASS_V_ERROR    [A-Za-z][a-zA-Z0-9_]*[a-zA-Z0-9]|[A-Za-z]
CLASS_V_ERROR2   [0-9][a-zA-Z0-9_]*[a-zA-Z0-9]|[0-9]
CLASS_V_ERROR3   [.^@][a-zA-Z0-9_]*

CHAR 		\\t|\\s|\\n|[^']
CHAR_ERROR	{CHAR}[^']


/* Gia kathe pattern (aristera) pou tairiazei ekteleitai o antistoixos
   kwdikas mesa sta agkistra. */


%%
"+"     {prn("PLUS",PLUS); return PLUS;}
"-"     {prn("MINUS",MINUS); return MINUS;}
"*"     {prn("MULTIPLE",MUL); return MUL;}
"/"     {prn("DIVISION",DIV); return DIV;}
"%"     {prn("MODULO",MOD); return MOD;}
"="     {prn("VARIABLE ASSNGMENT",ASSING); return ASSING;}
"**"        {prn("POWER",POWER); return POWER;}
"||"        {prn("LOGICAL OR",LOR); return LOR;}
"&&"        {prn("LOGICAL AND",LAND); return LAND;}
"=="        {prn("EQUAL",EQUAL); return EQUAL;}
">"        {prn("GREATER THAN",GRATERTHAN); return GRATERTHAN;}
"<"        {prn("LOWER THAN",LOWERTHAN); return LOWERTHAN;}
[\.]        {prn("DOT",DOT); return DOT;}
".."        {prn("DOUBLE DOTS",DOUBLE_DOTS); return DOUBLE_DOTS;}
">>"        {prn("LOGICAL SHIFT RIGHT",LSR); return LSR;}
"<<"        {prn("LOGICAL SHIFT LEFT",LSL); return LSL;}
"("	    {prn("LEFT PARENTHESIS",LPARENTHESIS); return LPARENTHESIS;}
")"	    {prn("RIGHT PARENTHESIS",RPARENTHESIS); return RPARENTHESIS;}
"\\"	    {prn("BACKSLASH",BACKSLASH); return BACKSLASH;}


"'"	    {BEGIN(character); return CHAR;}
"\""	    {BEGIN(string); return STRING;}


"class"         {prn("Class Declaration",CLASS); return CLASS;}
"loop"          {prn("loop",LOOP); return LOOP;}
"if"            {prn("if",IF); return IF;}
"elsif"         {prn("elsif",ELSIF); return ELSIF;}
"else"          {prn("else",ELSE); return ELSE;}
"until"         {prn("until",UNTIL); return UNTIL;}
"unless"        {prn("unless",UNLESS); return UNLESS;}
"return"        {prn("return",RETURN); return RETURN;}
"next"          {prn("next",NEXT); return NEXT;}
"begin"         {prn("begin",BEGIN2); return BEGIN2;}
"end"           {prn("end",END); return END;}
"break"         {prn("break",BREAK); return BREAK;}
"def"           {prn("def",DEF); return DEF;}

"print"         {prn("print",PRINT); return PRINT;}
"puts"          {prn("puts",PUTS); return PUTS;}
"gets"          {prn("gets",GETS); return GETS;}
"abort"         {prn("abort",ABORT); return ABORT;}
"chomp"         {prn("chomp",CHOMP); return CHOMP;}
"open"          {prn("open",OPEN); return OPEN;}

{DELIMITERS}    {}

"=begin"        {BEGIN(comment);return COMMENT;}
{LINE_COMMENT}  {return COMMENT;}
\n      	{line++;}

{LOCAL_V}   {prn("Local Variable",LOCAL_V); return LOCAL_V;}
{CONST_V}   {prn("Constant Variable",CONST_V); return CONST_V;}
{GLOBAL_V}  {prn("Global Variable",GLOBAL_V); return GLOBAL_V;}
{INST_V}    {prn("Instant Variable",INST_V); return INST_V;}
{CLASS_V}   {prn("Class Variable",CLASS_V); return CLASS_V;}



{INTEGER}   {prn("INTEGER",INTEGER); return INTEGER;}
{FLOAT}     {prn("Float",FLOAT); return FLOAT;}

{INTEGER_ERROR} {ERROR("Integer ends with letter!",INT_ERROR); BEGIN(error); return INT_ERROR;}
{INT_ERROR2} {ERROR("Integer has two sings!",INT_ERROR2); BEGIN(error); return INT_ERROR2;}
{INT_ERROR3} {ERROR("Integer is forbiten to begin with 0!",INT_ERROR3); BEGIN(error); return INT_ERROR3;}
{FLOAT_ERROR}   {ERROR("Float ends with letter!",FLOAT_ERROR); BEGIN(error); return FLOAT_ERROR;}
{FLOAT_ERROR2}   {ERROR("Float has two sings!",FLOAT_ERROR2); BEGIN(error); return FLOAT_ERROR2;}
{FLOAT_ERROR3}   {ERROR("Float is forbiten to begin with 0!",FLOAT_ERROR3); BEGIN(error); return FLOAT_ERROR3;}

{METHOD_ERROR}   {ERROR("Methods names can't start with upercase",METHOD_ERROR); BEGIN(error); return METHOD_ERROR;}
{METHOD_ERROR2}   {ERROR("Methods names can't start with numbers",METHOD_ERROR2); BEGIN(error); return METHOD_ERROR2;}
{METHOD_ERROR3}   {ERROR("Methods names can't contain underscore",METHOD_ERROR3); BEGIN(error); return METHOD_ERROR3;}
{METHOD_ERROR4}   {ERROR("Methods names may start only with characters a to z",METHOD_ERROR4); BEGIN(error); return METHOD_ERROR4;}

{LOCAL_V_ERROR}   {ERROR("Local variables names can't start with upercase",LOCAL_V_ERROR); BEGIN(error); return LOCAL_V_ERROR;}
{LOCAL_V_ERROR2}   {ERROR("Local variables names can't start with numbers",LOCAL_V_ERROR2); BEGIN(error); return LOCAL_V_ERROR2;}
{LOCAL_V_ERROR3}   {ERROR("Local variables names may start only with characters a to z",LOCAL_V_ERROR3); BEGIN(error); return LOCAL_V_ERROR3;}

{CONST_V_ERROR}   {ERROR("Constant variables names can't start with lowercase",CONST_V_ERROR); BEGIN(error); return CONST_V_ERROR;}
{CONST_V_ERROR2}   {ERROR("Constant variables names can't start with numbers",CONST_V_ERROR2); BEGIN(error); return CONST_V_ERROR2;}
{CONST_V_ERROR3}   {ERROR("Constant variables names may start only with characters A to Z",CONST_V_ERROR3); BEGIN(error); return CONST_V_ERROR3;}

{GLOBAL_V_ERROR}   {ERROR("Global variables names can't start with latters",GLOBAL_V_ERROR); BEGIN(error); return GLOBAL_V_ERROR;}
{GLOBAL_V_ERROR2}   {ERROR("Global variables names can't start with numbers",GLOBAL_V_ERROR2); BEGIN(error); return GLOBAL_V_ERROR2;}
{GLOBAL_V_ERROR3}   {ERROR("Global variables names may start only with '$'",GLOBAL_V_ERROR3); BEGIN(error); return GLOBAL_V_ERROR3;}

{INST_V_ERROR}   {ERROR("Instance variables names can't start with latters",INST_V_ERROR); BEGIN(error); return INST_V_ERROR;}
{INST_V_ERROR2}   {ERROR("Instance variables names can't start with numbers",INST_V_ERROR2); BEGIN(error); return INST_V_ERROR2;}
{INST_V_ERROR3}   {ERROR("Instance variables names may start only with '@@'",INST_V_ERROR3); BEGIN(error); return INST_V_ERROR3;}

{CLASS_V_ERROR}   {ERROR("Class variables names can't start with latters",CLASS_V_ERROR); BEGIN(error); return CLASS_V_ERROR;}
{CLASS_V_ERROR2}   {ERROR("Class variables names can't start with numbers",CLASS_V_ERROR2); BEGIN(error); return CLASS_V_ERROR2;}
{CLASS_V_ERROR3}   {ERROR("Class variables names may start only with '@'",CLASS_V_ERROR3); BEGIN(error); return CLASS_V_ERROR3;}

.       {BEGIN(error); return TOKEN_ERROR;}

<error>[ \t]    {BEGIN(0);}
<error>[\n] {line++; BEGIN(0);}
<error>.*    {printf("\tFlex -> UNKNOWN TOKEN at line : %d, %s\n", line, yytext);}

<comment>=end {BEGIN(0);}
<comment>[\n] {line++;}
<comment>.*	{}

<character>' {BEGIN(0);}
<character>\n {ERROR("CHARACTER: Unexpected new line",CHAR_ERROR); BEGIN(error); return CHAR_ERROR2;}
<character>{CHAR_ERROR}* {ERROR("CHARACTER: no more from one character",CHAR_ERROR); BEGIN(error); return CHAR_ERROR;}
<character>{CHAR} {prn("Character",CHAR);}

<string>\" {BEGIN(0);}
<string>\n {ERROR("STRING: Unexpected new line",STRING_ERROR); BEGIN(error); return STRING_ERROR;}
<string>[^"]* {prn("String",STRING);}

<<EOF>>     { printf("\t#End of file#\n"); exit(0); }

%%
int main(int argc, char **argv){
    int token;

        /* Ginetai o elegxos twn orismatwn ths grammhs entolwn. Ean ta
           orismata einai 3, to programma diabazei apo to arxeio tou 2ou
           orismatos kai grafei sto arxeio tou 3ou. Ean ta orismata einai
           2 diabazei apo to arxeio tou 2ou kai grafei sthn othonh.
           Ypenthymizetai oti to 1o orisma (argv[0]) sth C einai to onoma
           tou idiou tou ektelesimou arxeiou. */

    if(argc == 3){
        if(!(yyin = fopen(argv[1], "r"))) {
            fprintf(stderr, "Cannot read file: %s\n", argv[1]);
            return 1;
        }
        if(!(yyout = fopen(argv[2], "w"))) {
            fprintf(stderr, "Cannot create file: %s\n", argv[2]);
            return 1;
        }
    }
    else if(argc == 2){
        if(!(yyin = fopen(argv[1], "r"))) {
            fprintf(stderr, "Cannot read file: %s\n", argv[1]);
            return 1;
        }
    }

    /* H synarthsh yylex diabazei xarakthres apo thn eisodo kai prospathei
           na angnwrisei tokens. Ta tokens pou anagnwrizei einai auta pou exoun
           oristei sto paron arxeio, anamesa sta %% kai %%. An o kwdikas pou
           antistoixei se kapoio pattern periexei thn entolh 'return TIMH', h
       yylex() epistrefei thn timh auth h opoia kai apothhkeyetai sth
       metablhth token. */

    while( (token=yylex()) >= 0){
            /* Gia kathe token pou anagnwristhke, ektypwnetai h grammh pou auto
           brethhke kathws kai to onoma tou mazi me thn timh tou. */
       // fprintf(yyout, "\tLine=%d,  value=\"%s\"\n", line, yytext);
    }
    return 0;
}
/* H synarthsh typwnei sthn eksodo ena mhnyma lathous sthn periptwsh pou anagnwristei
   lanthasmeno token opws px. akeraios pou teleiwnei me gramma. */
void ERROR(const char *msg,int t)
{
        fprintf(yyout, "\tFlex -> Line %d,  at lexeme \'%s\' ERROR ID = %d: \n\tToken Error: %s\n\n",  line,yytext, t, msg);
}

/* H synarthsh typwnei sthn othonh to keimeno tou anagnwristikou pou tairiakse me kapoion
   kanona. Xrhsimopoieitai kathara gia logous katanohshs kai debugging. */
void prn(char *s,int t)
{

    printf("\tFlex -> At line %d, found %s (ID = %d , %s ) \n",line, yytext,t, s);
}






