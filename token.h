//SUMVOLA KAI TELESTES 1-20
#define PLUS         1
#define MINUS        2
#define MUL          3
#define DIV          4
#define MOD          5
#define ASSING       6
#define POWER        7
#define LOR          8
#define LAND         9
#define EQUAL        10
#define DOUBLE_DOTS  11
#define LSR          12
#define LSL          13
#define LPARENTHESIS 14
#define RPARENTHESIS 15
#define BACKSLASH    16
#define QUOTE	     17
#define DOUBLEQUOTE 18
#define GRATERTHAN   19
#define LOWERTHAN    20
#define DOT	     201


// METAVLHTES ARI8MOI METHODOI 21-39
#define INTCONST     21
#define FLOAT	     22
#define METHOD       23
#define LOCAL_V      24
#define CONST_V      25
#define GLOBAL_V     26
#define CLASS_V      27
#define INST_V       28
#define COMMENT      29
#define STRING       30
#define CHAR	     31

//KRATIMENES LEKSEIS 40-59
#define CLASS        40
#define LOOP         41
#define IF           42
#define ELSIF        43
#define ELSE         44
#define UNTIL        45
#define UNLESS       46
#define RETURN       47
#define NEXT         48
#define BEGIN2       49
#define END          50
#define BREAK        51
#define DEF          52

//PRWTARXIKES SUNARTISEIS 60-79
#define PRINT        60
#define PUTS         61
#define GETS         62
#define ABORT        63
#define CHOMP        64
#define OPEN         65

//ERRORS 80>
#define TOKEN_ERROR   81

#define INT_ERROR     82
#define INT_ERROR2    83
#define INT_ERROR3    84

#define FLOAT_ERROR   85
#define FLOAT_ERROR2  86
#define FLOAT_ERROR3  87

#define METHOD_ERROR  88
#define METHOD_ERROR2 89
#define METHOD_ERROR3 90
#define METHOD_ERROR4 91

#define LOCAL_V_ERROR  92
#define LOCAL_V_ERROR2 93
#define LOCAL_V_ERROR3 94

#define CONST_V_ERROR  95
#define CONST_V_ERROR2 96
#define CONST_V_ERROR3 97

#define GLOBAL_V_ERROR  98
#define GLOBAL_V_ERROR2 99
#define GLOBAL_V_ERROR3 100

#define INST_V_ERROR  101
#define INST_V_ERROR2 102
#define INST_V_ERROR3 103

#define CLASS_V_ERROR  104
#define CLASS_V_ERROR2 105
#define CLASS_V_ERROR3 106

#define STRING_ERROR 107
#define CHAR_ERROR 108
#define CHAR_ERROR2 109
